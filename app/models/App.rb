require 'open-uri'
class App
  attr_accessor :list

  def initialize(list)
    @list = list
  end

  def self.getInfo()
    uri = "https://rss.itunes.apple.com/api/v1/jp/ios-apps/top-grossing/all/200/explicit.json"
    json = open(uri) do |data|
      hash = JSON.load(data)
      App.new(hash["feed"]["results"])
    end
  end
end
